package com.hcll.surabhi.pojo;

import com.hcl.surabhi.presentation.Main;

public class Admin extends Thread {
	private String adminName;
	private String password;
	public Admin() {
		super();
	}
	public Admin(String name, String password) {
		super();
		this.adminName = name;
		this.password = password;
	}
	public String getAdminName() {
		return adminName;
	}
	public void setAdminName(String name) {
		this.adminName = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public void run() {
		Main.adminUI();
	}
	

}
