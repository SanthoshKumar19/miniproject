package com.hcll.surabhi.pojo;

public class Items {
	private int id;
	private String name;
	private int qty;
	private double cost;
	public Items() {
		super();
	}
	public Items(int id, String name, int qty, double cost) {
		super();
		this.id = id;
		this.name = name;
		this.qty = qty;
		this.cost = cost;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	@Override
	public String toString() {
		return "Items:-"+"\n"+"id:-"+id+"\n"+"Name:-"+name+"\n"+"Qty:-"+qty+"\n"+"Cost:-"+cost+"";
	}
	

}
