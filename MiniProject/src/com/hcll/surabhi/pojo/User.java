package com.hcll.surabhi.pojo;

import com.hcl.surabhi.presentation.Main;

public class User extends Thread {
	private String userName;
	private String email;
	private String password;
	public User() {
		super();
	}
	public User(String name, String email, String password) {
		super();
		this.userName = name;
		this.email = email;
		this.password = password;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String name) {
		this.userName = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "Username:-" + userName  ;
	}
	/*
	 * public void run() { Main.UserUI(); }
	 */
	
	
	
}
