package com.hcll.surabhi.pojo;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Orders {
	private int orderId;
	private String user;
	private ArrayList<Items> itemList;
	private LocalDate date;
	private double total;
	public Orders() {
		super();
	}
	public Orders(int orderId,String user, ArrayList<Items> itemList, LocalDate date, double total) {
		super();
		this.orderId=orderId;
		this.user = user;
		this.itemList = itemList;
		this.date = date;
		this.total = total;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public ArrayList<Items> getItemList() {
		return itemList;
	}
	public void setItemList(ArrayList<Items> itemList) {
		this.itemList = itemList;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	@Override
	public String toString() {
		return "Orders [orderId=" + orderId + ", user=" + user + ", itemList=" + itemList + ", date=" + date
				+ ", total=" + total + "]";
	}
	
	
	

}
