package com.hcl.surabhi.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import com.hcl.surabhi.doa.AdminDOA;
import com.hcll.surabhi.pojo.Items;
import com.hcll.surabhi.pojo.Orders;

public class AdminImpService implements IAdmin {
AdminDOA doa=new AdminDOA();
	@Override
	public boolean addItems(Items item) {
		// TODO Auto-generated method stub
		return doa.addItems(item);
	}

	@Override
	public List<Orders> todaysBill() {
		// TODO Auto-generated method stub
		return doa.todaysBill();
	}

	@Override
	public List<Orders> monthsBill() {
		// TODO Auto-generated method stub
		return doa.monthsBill();
	}

	@Override
	public List<Orders> totalBill() {
		// TODO Auto-generated method stub
		return doa.totalBill();
	}

}
