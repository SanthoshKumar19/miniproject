package com.hcl.surabhi.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.hcl.surabhi.exception.RestaurantException;
import com.hcll.surabhi.pojo.Admin;
import com.hcll.surabhi.pojo.Items;
import com.hcll.surabhi.pojo.Orders;

public interface IAdmin {
	public boolean addItems(Items item);
	public List<Orders> todaysBill();
	public List<Orders> monthsBill();
	public List<Orders> totalBill();
	
public static void validate(Admin admin) {
	Pattern pt=Pattern.compile("[a-zA-Z]{3,}[1-9]{5,}");
	if(!(pt.matcher(admin.getPassword()).matches())) {
		try {
			throw new RestaurantException();
		}catch(RestaurantException re) {
			re.getInvalidPass();
			System.exit(0);
		}
	}
}
public static void validateBillReq(List<Orders> bills) {
	if(bills.isEmpty()) {
		try {
			throw new RestaurantException();
		}catch(RestaurantException re) {
			re.noBills();
		}
	}
}
}
