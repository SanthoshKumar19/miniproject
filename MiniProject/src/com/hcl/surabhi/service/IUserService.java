package com.hcl.surabhi.service;

import java.util.List;
import java.util.regex.Pattern;

import com.hcl.surabhi.exception.RestaurantException;
import com.hcll.surabhi.pojo.Items;
import com.hcll.surabhi.pojo.Orders;
import com.hcll.surabhi.pojo.User;

public interface IUserService {
	public boolean addItems(int selcetedItem);
	public List<Items> showItems();
	public Orders placeOrder(String userName);
	
public static void validate(User user) {
	Pattern pt=Pattern.compile("[a-zA-Z]{3,}[1-9]{5,}");
	Pattern ptmail=Pattern.compile("[a-zA-Z]+@[a-zA-Z]+.com");
	if(!(ptmail.matcher(user.getEmail()).matches())){
		try {
			throw new RestaurantException();
		}catch(RestaurantException re) {
			re.getInvalidMail();
			
	}
	}
	if(!(pt.matcher(user.getPassword()).matches())) {
		try {
			throw new RestaurantException();
		}catch(RestaurantException re) {
			re.getInvalidPass();
			System.exit(0);
		}
		
	}
	
	}
public static void validateBillReq(Orders currentOrder) {
	if(currentOrder==null) {
		try {
			throw new RestaurantException();
		}catch(RestaurantException re){
			re.emptSelected();
		}
	}
}
public static void shoItemExcep(List<Items> show) {
	if(show.isEmpty()) {
		try {
			throw new RestaurantException();
		}catch(RestaurantException re) {
			re.closedException();
		}
	}
}
}
