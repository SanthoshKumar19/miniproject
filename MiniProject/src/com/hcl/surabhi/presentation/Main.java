package com.hcl.surabhi.presentation;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import com.hcl.surabhi.service.AdminImpService;
import com.hcl.surabhi.service.IAdmin;
import com.hcl.surabhi.service.IUserService;
import com.hcl.surabhi.service.UserImp;
import com.hcll.surabhi.pojo.Admin;
import com.hcll.surabhi.pojo.Items;
import com.hcll.surabhi.pojo.Orders;
import com.hcll.surabhi.pojo.User;
public class Main {
	

	 
	static Scanner sc=new Scanner(System.in);
	public static void adminUI() {
		IAdmin adminImp =new AdminImpService();
		System.out.println("Enter your Admin Name");
		String adminname=sc.nextLine();
		System.out.println("Enter your password:");
		String adminPass=sc.nextLine();
		IAdmin.validate(new Admin(adminname,adminPass));
		System.out.println("Welcome Admin");
		System.out.println("*************");
		
		int choice;
		do{
			System.out.println("1.To see today's bills\n2.To see bills of this month\n3.To see all the bills\n4.Add items\n5.Exit");
			 choice=sc.nextInt();
		
		sc.nextLine();
		switch(choice) {
		case 1:{
			List<Orders>todaysBill=adminImp.todaysBill();
			IAdmin.validateBillReq(todaysBill);
			for(Orders o:todaysBill) {
				System.out.println(o.getOrderId());
				System.out.println(o.getUser());
				System.out.println(o.getItemList());
				System.out.println(o.getDate());	
				System.out.println("Total: "+o.getTotal());
				}
			break;
		}
		case 2:{
			List<Orders>monthsBill=adminImp.monthsBill();
			IAdmin.validateBillReq(monthsBill);
			for(Orders o:monthsBill) {
				System.out.println(o.getOrderId());
				System.out.println(o.getUser());
				System.out.println(o.getItemList());
				System.out.println(o.getDate());	
				System.out.println("Total: "+o.getTotal());
				}
			break;
		}
		case 3:{
			List<Orders>totalBill=adminImp.totalBill();
			IAdmin.validateBillReq(totalBill);
			for(Orders o:totalBill) {
				System.out.println(o.getOrderId());
				System.out.println(o.getUser());
				System.out.println(o.getItemList());
				System.out.println(o.getDate());	
				System.out.println("Total: "+o.getTotal());
				}
			break;
		}
		case 4:{
			System.out.println("Enter the id of the item:");
			int id=sc.nextInt();
			sc.nextLine();
			System.out.println("Enter the name:");
			String name=sc.nextLine();
			System.out.println("Enter the quantity");
			int qty=sc.nextInt();
			System.out.println("Enter the cost of the item:");
			double cost=sc.nextDouble();
			if(adminImp.addItems(new Items(id,name,qty,cost))) {
				System.out.println("Item added successfully\n");
			}
			else {
				System.err.println("Addition of item failed\n");
			}
			break;
		}
		
		}
		}while(choice!=5);
		System.out.println("Thank you");
		
	}
	public static void UserUI() {
		UserImp userImp=new UserImp();
		String userName=new String();
		System.out.println("Enter your user name:");
		 userName=sc.nextLine();
		System.out.println("Enter your email id:");
		String email=sc.nextLine();
		System.out.println("Enter your password:");
		String userPass=sc.nextLine();
		IUserService.validate(new User(userName,email,userPass));
		int itemOpt;
		System.out.println("Welcome to surabhi restaurant "+new User(userName,email,userPass).getUserName());
		do {
			
		System.out.println("Enter the id to add food\n0.To place the order");
		List<Items> items=userImp.showItems();
		IUserService.shoItemExcep(items);
		for(Items i:items) {
			System.out.println(i);
		}
		itemOpt=sc.nextInt();
		
		userImp.addItems(itemOpt);
		
		}while (itemOpt!=0);
		Orders currentOrder=userImp.placeOrder(userName);
		IUserService.validateBillReq(currentOrder);
		System.out.println("User name: "+currentOrder.getUser());

		System.out.println("Items ordered: ");
		List<Items> orderedList=currentOrder.getItemList();
		for(Items i:orderedList) {
			System.out.println("Item name: "+i.getName());
			System.out.println("Quantity: "+i.getQty());
			System.out.println("Cost: "+i.getCost());
		}
		System.out.println("Your total is : "+currentOrder.getTotal()+"\n\n");
		
		currentOrder.getItemList().clear();
		
		}

	public static void main(String[] args) {
		
		
		String choice;
		
		while(true){
			System.out.println("Welcome to Surabhi Restaurant");
			System.out.println("*****************************");
			System.out.println("Enter U if you are a user\nEnter A if you are an admin\n-1 To exit");
			 choice=sc.nextLine();
		
		switch(choice) {
		case "A":{
			Main.adminUI();
			
			break;
		}
		case "U":{
			
			Main.UserUI();
			break;
		}
		case "-1":{
			System.out.println("Thank you");
			
			System.exit(0);
			break;
		}
		}
		}
		

	}

}
