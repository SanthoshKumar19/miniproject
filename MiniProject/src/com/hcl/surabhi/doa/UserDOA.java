package com.hcl.surabhi.doa;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hcll.surabhi.pojo.Items;
import com.hcll.surabhi.pojo.Orders;

public   class UserDOA implements IUserDOA {
	
	static Map<Integer,Orders> orders=new HashMap<Integer,Orders>();
	static ArrayList<Items> selectedList=new ArrayList<Items>();
	 static  List<Items>itemsList=new ArrayList<Items>();
	 static int orderId=1;
	@Override
	public boolean addItems(int selcetedItem) { //Adding items that the user is chooisng
		boolean res=false;
		for(Items i:itemsList) {
			if(i.getId()==selcetedItem) {
				selectedList.add(i);
				res=true;
				break;
			}
			
		}
		return res;
	}

	@Override
	public List<Items> showItems() {
		
		return itemsList;
	}

	@Override
	public Orders placeOrder(String userName)  { //Calculating the total of the user's order and writting it in a file and displaying the bill to the user as well
		boolean res=false;
		LocalDate date=LocalDate.now();
		double total=0;
	 total=selectedList.stream().mapToDouble(d -> d.getCost()).sum();
	Orders order=new Orders(orderId,userName,selectedList,date,total);
	orders.put(orderId, order);
	orderId+=1;
	try {
		FileWriter writer =new FileWriter(new File("src/Bills.txt"),true);
		writer.write("order id:- "+order.getOrderId()+"\n");
		writer.write("User:- "+order.getUser()+"\n");
//		writer.write(order.getItemList()+"\n");
		List<Items> itemList=order.getItemList();
		writer.write("Items ordered:-"+"\n");
		for(Items i:itemList) {
			writer.write("Item name: "+i.getName()+"\n");
			writer.write("Quantity: "+i.getQty()+"\n");
			writer.write("Cost: "+i.getCost()+"\n");
			
		}
		writer.write("Date:-"+order.getDate()+"\n");
		writer.write("Total:- "+order.getTotal()+"\n"+"\n");
		writer.flush();
		
	} catch (IOException e) {
		
		e.printStackTrace();
	}
		return order;
	}

}
