package com.hcl.surabhi.doa;

import java.util.List;

import com.hcll.surabhi.pojo.Items;
import com.hcll.surabhi.pojo.Orders;

public interface IUserDOA {
	public boolean addItems(int selcetedItem);
	public List<Items> showItems();
	public Orders placeOrder(String userName);
}
