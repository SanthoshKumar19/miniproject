package com.hcl.surabhi.doa;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import com.hcll.surabhi.pojo.Items;
import com.hcll.surabhi.pojo.Orders;

public interface IAdminDOA {
	public boolean addItems(Items item);
	public List<Orders> todaysBill();
	public List<Orders> monthsBill();
	public List<Orders> totalBill();
}
