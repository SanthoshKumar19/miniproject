package com.hcl.surabhi.doa;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.hcll.surabhi.pojo.Items;
import com.hcll.surabhi.pojo.Orders;

public class AdminDOA implements IAdminDOA {
	UserDOA userDoa=new UserDOA();
	
	@Override
	public boolean addItems(Items item) {
		boolean res=false;
		if(userDoa.itemsList.add(item)) {
			res=true;
		}
		//System.out.println(userDoa.itemsList);
		return res;
	}

	@Override
	public List<Orders> todaysBill() {
		LocalDate today=LocalDate.now();
		List<Orders> todayOrders=userDoa.orders.values().stream().filter(d -> d.getDate().equals(today)).collect(Collectors.toList());
//		
		return todayOrders;	
		}

	@Override
	public List<Orders> monthsBill() {
		LocalDate today=LocalDate.now();
		List<Orders> monthOrders=userDoa.orders.values().stream().filter(d -> d.getDate().getMonth().equals(today.getMonth())).collect(Collectors.toList());
		return monthOrders;	
		
	}

	@Override
	public List<Orders> totalBill() {
		List<Orders> totalBillsList=userDoa.orders.values().stream().toList();
		return totalBillsList;
	}

}
