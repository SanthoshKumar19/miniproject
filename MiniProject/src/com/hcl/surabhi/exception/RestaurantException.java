package com.hcl.surabhi.exception;

public class RestaurantException extends Exception {
	public void getInvalidPass() {
		System.err.println(" Password should be atleast 8 charachters of 3 minimum alpha char and of 5 minimum numeric char no special chars allowed ");
	}
	 public void closedException() {
		 System.err.println("Sorry we are closed today");
	 }
	 
	 public void emptSelected() {
		 System.err.println("No items selected\n");
	 }
	 public void noBills() {
		 System.err.println("No bills under this category\n");
	 }
	 public void getInvalidMail() {
		System.err.println("Invalid mail id");
	 }
}
